﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatServer
{
    public class Message
    {
        private string Text { get; }
        private int ReadCount { get; set; }

        public Message(string text)
        {
            Text = text;
            ReadCount = 0;
        }
    }
}
