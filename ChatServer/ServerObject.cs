﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace ChatServer
{
    public class ServerObject
    {
        private static TcpListener _tcpListener; // сервер для прослушивания
        private readonly List<ClientObjectReceiver> _clients = new List<ClientObjectReceiver>(); // все подключения
        private readonly CircularBuffer<string> _chatHistory;

        /// <param name="bufferSize">Максимальное количество сообщений, хранящееся на сервере</param>
        public ServerObject(int bufferSize)
        {
            _chatHistory = new CircularBuffer<string>(bufferSize);
        }

        protected internal void AddConnection(ClientObjectReceiver clientObject)
        {
            _clients.Add(clientObject);
        }
        protected internal void RemoveConnection(string id)
        {
            // получаем по id закрытое подключение
            ClientObjectReceiver client = _clients.FirstOrDefault(c => c.Id == id);
            // и удаляем его из списка подключений
            if (client != null)
                _clients.Remove(client);
        }

        // прослушивание входящих подключений
        protected internal void Listen()
        {
            try
            {
                _tcpListener = new TcpListener(IPAddress.Any, 8888);
                _tcpListener.Start();
                Console.WriteLine("Сервер запущен. Ожидание подключений...");

                while (true)
                {
                    TcpClient tcpClient = _tcpListener.AcceptTcpClient();

                    var clientObject = new ClientObjectReceiver(tcpClient, this);
                    var clientThread = new Thread(clientObject.Process);
                    clientThread.Start();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Disconnect();
            }
        }

        protected internal CircularBuffer<string> GetHistory()
        {
            return _chatHistory;
        }

        protected internal void BroadcastHistory(string id){
            
            var client = _clients.Find(c => c.Id == id);
            string chatHistory = "";
            for (int i = 0; i < _chatHistory.Size; i++)
            {
                if (chatHistory != "") chatHistory += "\n";
                chatHistory += _chatHistory[i];
            }
            byte[] data = Encoding.Unicode.GetBytes(chatHistory);
            client.Stream.Write(data, 0, data.Length);
        }
        
        protected internal void BroadcastMessage(string message, string id, bool sendToMe = true)
        {
            _chatHistory.PushBack(message);
            byte[] data = Encoding.Unicode.GetBytes(message);
            var clients = _clients;
            if (!sendToMe) clients = _clients.FindAll(client => client.Id != id);
            clients.ForEach(client =>
            {
                try
                {
                    client.Stream.Write(data, 0, data.Length);
                }
                catch (IOException)
                {
                    
                }
            });
        }
        
        protected internal void Disconnect()
        {
            _tcpListener.Stop(); // остановка сервера
            _clients.ForEach(client => client.Close()); // отключение клиентов
            Environment.Exit(0); // завершение процесса
        }
    }
}
