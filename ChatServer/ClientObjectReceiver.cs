﻿using System;
using System.Net.Sockets;
using System.Text;

namespace ChatServer
{
    public class ClientObjectReceiver
    {
        protected internal string Id { get; }
        protected internal NetworkStream Stream { get; private set; }
        private string _userName;
        private readonly TcpClient _client;
        private readonly ServerObject _server; // объект сервера

        public ClientObjectReceiver(TcpClient tcpClient, ServerObject serverObject)
        {
            Id = Guid.NewGuid().ToString();
            _client = tcpClient;
            _server = serverObject;
            serverObject.AddConnection(this);
        }

        public void Process()
        {
            try
            {
                Stream = _client.GetStream();

                // Получаем историю сообщений
                _server.BroadcastHistory(Id);

                // Получаем имя пользователя
                _userName = GetMessage();
                var message = _userName + " вошел в чат";
                // Посылаем сообщение о входе в чат всем подключенным пользователям
                _server.BroadcastMessage(message, Id);
                Console.WriteLine(message);
                // В бесконечном цикле получаем сообщения от клиента
                while (true)
                {
                    try
                    {
                        message = GetMessage();
                        message = $"{_userName}: {message}";
                        Console.WriteLine(message);
                        _server.BroadcastMessage(message, Id);
                    }
                    catch
                    {
                        message = $"{_userName}: покинул чат";
                        Console.WriteLine(message);
                        _server.BroadcastMessage(message, Id);
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                // В случае выхода из цикла закрываем ресурсы
                _server.RemoveConnection(Id);
                Close();
            }
        }

        /// <summary>
        /// Чтение входящего сообщения и преобразование в строку
        /// </summary>
        /// <returns></returns>
        private string GetMessage()
        {
            var data = new byte[64]; // буфер для получаемых данных
            var builder = new StringBuilder();
            do
            {
                var bytes = Stream.Read(data, 0, data.Length);
                builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
            }
            while (Stream.DataAvailable);

            return builder.ToString();
        }

        /// <summary>
        /// Закрытие подключения
        /// </summary>
        protected internal void Close()
        {
            Stream?.Close();
            _client?.Close();
        }
    }
}
