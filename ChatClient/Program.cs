﻿using System;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace ChatClient
{
    internal class Program
    {
        private static string _userName;
        private const string Host = "127.0.0.1";
        private const int Port = 8888;
        private static TcpClient _client;
        private static NetworkStream _stream;

        static void Main(string[] args)
        {
            Console.Write("Введите свое имя: ");
            _userName = Console.ReadLine();
            _client = new TcpClient();
            try
            {
                _client.Connect(Host, Port); //подключение клиента
                _stream = _client.GetStream(); // получаем поток

                var message = _userName;
                byte[] data = Encoding.Unicode.GetBytes(message);
                _stream.Write(data, 0, data.Length);

                // запускаем новый поток для получения данных
                var receiveThread = new Thread(ReceiveMessage);
                receiveThread.Start(); // старт потока
                SendMessage();
            }
            catch (SocketException)
            {
                Console.WriteLine("Не удалось подключиться к серверу.");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Disconnect();
            }
        }
        // отправка сообщений
        static void SendMessage()
        {
            while (true)
            {
                var message = "";
                while (true)
                {
                    var key = Console.ReadKey(true);
                    if (key.Key == ConsoleKey.Enter)
                        break;
                    message += key.KeyChar;
                }
                var data = Encoding.Unicode.GetBytes(message);
                _stream.Write(data, 0, data.Length);
            }
        }
        // получение сообщений
        private static void ReceiveMessage()
        {
            while (true)
            {
                try
                {
                    var data = new byte[64]; // буфер для получаемых данных
                    var builder = new StringBuilder();
                    do
                    {
                        var bytes = _stream.Read(data, 0, data.Length);
                        builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
                    }
                    while (_stream.DataAvailable);

                    var message = builder.ToString();
                    Console.WriteLine(message);//вывод сообщения
                }
                catch
                {
                    Console.WriteLine("Подключение прервано!"); //соединение было прервано
                    Console.ReadLine();
                    Disconnect();
                }
            }
        }

        private static void Disconnect()
        {
            _stream?.Close(); //отключение потока
            _client?.Close(); //отключение клиента
            Environment.Exit(0); //завершение процесса
        }
    }
}
